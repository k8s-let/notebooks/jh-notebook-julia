FROM jupyter/datascience-notebook:hub-2.3.1

USER root

# Install ETHZ root ca certificates
ADD http://pkiaia.ethz.ch/aia/ETHZRootCA2020.pem /usr/local/share/ca-certificates/
ADD http://pkiaia.ethz.ch/aia/ETHZIssuingCA2020.pem /usr/local/share/ca-certificates/
ADD http://pkiaia.ethz.ch/aia/DigiCertGlobalRootCA.pem /usr/local/share/ca-certificates/DigiCertGlobalRootCA.crt
ADD http://pkiaia.ethz.ch/aia/DigiCertTLSRSASHA2562020CA1-1.pem /usr/local/share/ca-certificates/DigiCertTLSRSASHA2562020CA1-1.crt
RUN chmod 644 /usr/local/share/ca-certificates/ETHZ* && update-ca-certificates

RUN apt-get update && apt-get upgrade -y && apt-get install -y zip less vim man

RUN conda install nbgitpuller

RUN http_proxy=http://proxy.ethz.ch:3128 https_proxy=http://proxy.ethz.ch:3128 julia -e "using Pkg; Pkg.add.([ \
	\"NBInclude\", \ 
	\"PyPlot\", \ 
	\"Roots\", \
	\"UnPack\", \ 
	\"Plots\" \
]); Pkg.update;"

# Fix timezone
RUN ln -sf /usr/share/zoneinfo/Europe/Zurich /etc/localtime && echo "Europe/Zurich" >> /etc/timezone

USER 1000
